package md593ff26953b05b9f61a75dff7228e0e74;


public class HomeView
	extends mvvmcross.droid.support.v4.MvxFragmentActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("sample.droid.Views.HomeView, sample.droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", HomeView.class, __md_methods);
	}


	public HomeView () throws java.lang.Throwable
	{
		super ();
		if (getClass () == HomeView.class)
			mono.android.TypeManager.Activate ("sample.droid.Views.HomeView, sample.droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
