using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using Android.Support.V4.View;
using com.refractored;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Sample.Core.ViewModels;
using moneycareapp.droid.Adapters;
using sample.droid.fragment;

namespace sample.droid.Views
{
    [Activity(Label = "HomeView", MainLauncher =true)]
    public class HomeView : MvxFragmentActivity
    {
        private ViewPager _viewPager;

        private PagerSlidingTabStrip _pageIndicator;

        List<MvxViewPagerFragmentAdapter.FragmentInfo> fragments;

        DrawerLayout drawerLayout;


        public new HomeViewModel ViewModel
        {
            get { return (HomeViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.home);

           


            fragments = new List<MvxViewPagerFragmentAdapter.FragmentInfo>
              {
                new MvxViewPagerFragmentAdapter.FragmentInfo
              {
                  FragmentType = typeof(SchoolFragment),
                 Title = "     School     ",
              ViewModel = ViewModel.school
                },
              new MvxViewPagerFragmentAdapter.FragmentInfo
             {
                 FragmentType = typeof(HospitalFragment),
               Title = "     Hospital     ",
                ViewModel = ViewModel.hospital
              },
               new MvxViewPagerFragmentAdapter.FragmentInfo
            {
               FragmentType = typeof(OfficeFragment),
                Title = "     Office    ",
                  ViewModel = ViewModel.office
                }
              };




            _viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            MvxViewPagerFragmentAdapter _adapter = new MvxViewPagerFragmentAdapter(this, SupportFragmentManager, fragments);
            _viewPager.Adapter = _adapter;

          



            _pageIndicator = FindViewById<PagerSlidingTabStrip>(Resource.Id.viewPagerIndicator);

            _pageIndicator.SetViewPager(_viewPager);

        }


    
    }
}

