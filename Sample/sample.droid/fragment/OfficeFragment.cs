using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Binding.Droid.BindingContext;

namespace sample.droid.fragment
{
    [Register("sample.droid.fragment.OfficeFragment")]
    public class OfficeFragment : MvxFragment
        {

            public OfficeFragment()
            {

            }

            public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
            {
                var ignore = base.OnCreateView(inflater, container, savedInstanceState);
                return this.BindingInflate(Resource.Layout.Office, null);
            }
        }
    }
