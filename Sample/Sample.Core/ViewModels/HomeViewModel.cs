using MvvmCross.Core.ViewModels;

namespace Sample.Core.ViewModels
{
   
        public class HomeViewModel : MvxViewModel
        {
            public SchoolViewModel school { get; set; }
            public HospitalViewModel hospital { get; set; }
            public OfficeViewModel office { get; set; }

            public HomeViewModel()
            {
                school = new SchoolViewModel();
                hospital = new HospitalViewModel();
                office = new OfficeViewModel();
            }

        }

    }
    

